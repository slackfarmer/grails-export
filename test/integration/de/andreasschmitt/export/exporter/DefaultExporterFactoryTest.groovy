package de.andreasschmitt.export.exporter

import org.codehaus.groovy.grails.plugins.testing.GrailsMockHttpServletResponse

import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors

/**
 * Created by ccollins on 5/15/14.
 */
class DefaultExporterFactoryTest extends GroovyTestCase {
    def exportService

    void setUp() {
        super.setUp()
    }

    void tearDown() {

    }

    void testCreateExporterOutputStream() {
        def baos = new ByteArrayOutputStream()

        def results = [
                        ["orderSerialNo"   : "orderS┼erialNo", "jobId": "jobId", "fulfillerOrderNo": "fulfillerOrderNo",
                        "brand": "brand", "fulfiller"       : "fulfiller", "orderDate": "orderDate",
                        "orderTime": "orderTime", "jobDate": "jobDate", "jobTime": "jobTime", "shipDate" : "shipDate",
                        "shipTime": "shipTime", "shipmentNo": "shipmentNo", "prodType" : "prodType",
                        "prodSku": "prodSku", "formFactor": "formFactor", "properties": "properties",
                        "selectShipMethod": "selectShipMethod", "actualMethod": "actualMethod",
                        "currentState": "currentState", "shipCountry": "shipCountry", "shipRegion" : "shipRegion",
                        "shipPostCode": "shipPostCode", "superRush": "superRush", "ldd": "ldd",
                        "daysToLdd": "daysToLdd", "total": "total", "numDays": "numDays"],
                       ["orderSerialNo"   : "orderSerialNo", "jobId": "jobId", "fulfillerOrderNo": "fulfillerOrderNo",
                        "brand": "brand", "fulfiller"       : "fulfiller", "orderDate": "orderDate",
                        "orderTime": "orderTime", "jobDate": "jobDate", "jobTime": "jobTime", "shipDate" : "shipDate",
                        "shipTime": "shipTime", "shipmentNo": "shipmentNo", "prodType" : "prodType",
                        "prodSku": "prodSku", "formFactor": "formFactor", "properties": "properties",
                        "selectShipMethod": "selectShipMethod", "actualMethod": "actualMethod",
                        "currentState": "currentState", "shipCountry": "shipCountry", "shipRegion" : "shipRegion",
                        "shipPostCode": "shipPostCode", "superRush": "superRush", "ldd": "ldd",
                        "daysToLdd": "daysToLdd", "total": "total", "numDays": "numDays"]]

        def upperCase = { value ->
            return value.toString().toUpperCase()
        }

        List fields = [
                "orderSerialNo",
                "jobId",
                "fulfillerOrderNo",
                "brand",
                "fulfiller",
                "orderDate",
                "orderTime",
                "jobDate",
                "jobTime",
                "shipDate",
                "shipTime",
                "shipmentNo",
                "prodType",
                "prodSku",
                "formFactor",
                "properties",
                "selectShipMethod",
                "actualMethod",
                "currentState",
                "shipCountry",
                "shipRegion",
                "shipPostCode",
                "superRush",
                "ldd",
                "daysToLdd",
                "total",
                "numDays"
        ]

        Map labels = ["orderSerialNo"   : "orderSerialNo", "jobId": "jobId", "fulfillerOrderNo": "fulfillerOrderNo", "brand": "brand",
                      "fulfiller"       : "fulfiller", "orderDate": "orderDate", "orderTime": "orderTime", "jobDate": "jobDate", "jobTime": "jobTime",
                      "shipDate"        : "shipDate", "shipTime": "shipTime", "shipmentNo": "shipmentNo",
                      "prodType"        : "prodType", "prodSku": "prodSku", "formFactor": "formFactor", "properties": "properties",
                      "selectShipMethod": "selectShipMethod", "actualMethod": "actualMethod", "currentState": "currentState", "shipCountry": "shipCountry",
                      "shipRegion"      : "shipRegion", "shipPostCode": "shipPostCode", "superRush": "superRush", "ldd": "ldd", "daysToLdd": "daysToLdd", "total": "total", "numDays": "numDays"]

        Map formatters = [status: upperCase]
        Map parameters = [title: "Detailed Pending Jobs Report", "column.widths": [
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370,
                0.0370
        ]]

        exportService.export("excel", baos, results, fields, labels, formatters, parameters)

        def fos = new FileOutputStream("/testCreateExporterFromOutputStream.xlsx")
        baos.writeTo(fos)
        fos.close()
        baos.close()
    }

    void testCreateExporterServletResponse() {
        def executor = Executors.newFixedThreadPool(50)
        def n = 20
        def cdl = new CountDownLatch(n)
        for (i in 0..<n) {
            executor.execute {
                runExporterServletResponseCode()

                cdl.countDown()
            }
        }
        cdl.await()
    }

    private runExporterServletResponseCode(num){
        def response = new GrailsMockHttpServletResponse()

        // Formatter closure
        def upperCase = { value ->
            return value.toString().toUpperCase()
        }

        def fields = [
                "bob",
                "dobbs",
                "eris",
                "shipDate",
                "shipCal",
                "iAmNull",
                "testZero"
        ]

        Map labels = ["bob": "bob", "dobbs": "dobbs", "eris": "eris", "shpDate": "shipDate", "shipCal": "shipCal", "iAmNull": "iAmNull", "testZero": "testZero"]
        Map formatters = [status: upperCase]
        Map parameters = [title: "Sheet Name Goes Here", "column.widths": [
                0.1429,
                0.1429,
                0.1429,
                0.1429,
                0.1429,
                0.1429,
                0.1429
        ]]

        def data = (1..101).collect {
            if (it == 101) {
                def badString = """
CARD_MFG_WORKFLOW=STATIONERY|FOIL_CARD_DOCUMENT=https://services.tinyprints.com/print_file.htm?method=getPrintFile&printFileName=318597663_14525380_54874_fr_composite.pdf|FOIL_COLOR=SILVER|FOIL_DESIGN=SIGNED_TRIUMPH|PAPER_TYPE=SIGNATURE_MATTE|TRIM=SQUARE
"""
                new Expando(["bob": "Bob - 101", "dobbs": "102", "eris": "${badString}", "shipDate": new Date(), "shipCal": Calendar.getInstance(), "iAmNull": null, "testZero": -23])
            } else {
                new Expando(["bob": "Bob - ${it}", "dobbs": "${it}", "eris": "${it}", "shipDate": new Date(), "shipCal": Calendar.getInstance(), "iAmNull": null, "testZero": 0])
            }
        }

        exportService.export("excel", response, "/testCreateExporterFromServletResponse"+num, "xlsx", data, fields, labels, formatters, parameters)

        def fileName = response.getHeader("Content-disposition").split("=")[1]

        def fos = new FileOutputStream(fileName)
        fos.write(response.getContentAsByteArray())
        fos.close()
    }

    void testCreateTempFile() {
        def executor = Executors.newFixedThreadPool(10)
        def n = 50
        def cdl = new CountDownLatch(n)
        for (i in 0..<n) {
            executor.execute {
                def f = File.createTempFile("sheet", ".xml")
                //f.deleteOnExit()
                cdl.countDown()
            }
        }
        cdl.await()
    }

    void testCreateExporter2() {

    }

    void testCreateExporter3() {

    }
}
