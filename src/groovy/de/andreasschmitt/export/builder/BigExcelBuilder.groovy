package de.andreasschmitt.export.builder

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFDataFormat
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.sql.SQLTimeoutException
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

class BigExcelBuilder extends BuilderSupport {
    def baos
    def zos
    def styles
    def workbook
    def sheetWriter
    SpreadsheetWriter spreadSheetWriter

    String format
    Map formats = [:]

    def static final XML_ENCODING = "UTF-8"

    private static Log log = LogFactory.getLog(ExcelBuilder)

    /**
     * This method isn't implemented.
     */
    protected void setParent(Object parent, Object child) {}

    /**
     * This method is invoked when you invoke a method without arguments on the builder
     * e.g. builder.write().
     *
     * @param name The name of the method which should be invoked e.g. write.
     *
     */
    protected Object createNode(Object name) {
        log.debug("createNode(Object name)")
        log.debug("name: ${name}")

        if (name == "write") {
            // closes open resources
            write()
        }

        return null
    }

    /**
     * This method isn't implemented.
     */
    protected Object createNode(Object name, Object value) {
        log.debug("createNode(Object name, Object value)")
        log.debug("name: ${name} value: ${value}")
        return null
    }

    /**
     * This method is invoked when you invoke a method with a map of attributes e.g.
     * cell(row: 0, column: 0, value: "Hello1"). It switches between different build
     * actions such as creating the workbook, sheet, cells etc.
     *
     * @param name The name of the method which should be invoked e.g. cell
     * @param attributes The map of attributes which have been supplied e.g. [row: 0, column: 0, value: "Hello1"]
     *
     */
    protected Object createNode(Object name, Map attributes) {
        log.debug("createNode(Object name, Map attributes)")
        log.debug("name: ${name} attributes: ${attributes}")

        switch (name) {
            case "workbook":
                // Workbook, the Excel document as such
                if (attributes?.outputStream) {
                    try {
                        log.debug("Creating workbook")
                        workbook = new XSSFWorkbook()
                        baos = attributes?.outputStream
                        zos = new ZipOutputStream(baos)
                    } catch (e) {
                        log.error("Error creating workbook", e)
                        throw e
                    }
                } else {
                    throw new IllegalArgumentException("Export service requires user supplied outputstream!")
                }
                break
            case "sheet":
                // Sheet, an Excel file can contain multiple sheets which are typically shown as tabs
                try {
                    def sheet = workbook.createSheet(attributes?.name?.replaceAll(/[:*'\[\]?\/\\]+/, ' '))

                    styles = createStyles(workbook)

                    //name of the zip entry holding sheet data, e.g. xl/worksheets/sheet1.xml
                    def sheetEntryRef = sheet.getPackagePart().getPartName().getName().substring(1);

                    //write template out to stream
                    def os = new ByteArrayOutputStream()
                    workbook.write(os)

                    // turn template output stream into zipInputStream
                    def zip = new ZipInputStream(new ByteArrayInputStream(os.toByteArray()))

                    def zipEntry
                    //copies everything from template except sheet.xml
                    while ((zipEntry = zip.getNextEntry()) != null) {
                        if (!zipEntry.getName().equals(sheetEntryRef)) {
                            zos.putNextEntry(new ZipEntry(zipEntry.getName()))
                            copyStream(zip, zos)
                        } else {
                            copyStream(zip, new ByteArrayOutputStream())
                        }
                    }

                    // adds sheet.xml to the the zip stream
                    zos.putNextEntry(new ZipEntry(sheetEntryRef))
                    sheetWriter = new OutputStreamWriter(zos, XML_ENCODING)

                    spreadSheetWriter = new SpreadsheetWriter(sheetWriter);
                    spreadSheetWriter.beginSheet();
                } catch (Exception e) {
                    log.error("Error creating sheet", e)
                    throw e
                }
                break
            case "rowStart":
                try {
                    int rowIndex = attributes?.row as int
                    //insert header row
                    spreadSheetWriter.insertRow(rowIndex)
                } catch (Exception e) {
                    log.error("Error starting row", e)
                    throw e
                }
                break;
            case "rowEnd":
                try {
                    int rowIndex = attributes?.row as int
                    spreadSheetWriter.endRow()
                } catch (Exception e) {
                    log.error("Error ending row", e)
                    throw e
                }
                break;
            case "cell":
                try {
                    // Cell, column header or row cells
                    int rowIndex = attributes?.row as int
                    int columnIndex = attributes?.column as int
                    def columnValue = attributes?.value != null ? attributes?.value : ""
                    def columnFormat = attributes?.format as String

                    int styleIndex = styles.get("header").getIndex();
                    if (columnFormat == 'header') {
                        spreadSheetWriter.createCell(columnIndex, columnValue, styleIndex)
                    } else {
                        if (columnValue instanceof Number) {
                            spreadSheetWriter.createCell(columnIndex, columnValue)
                        } else if (columnValue instanceof Date) {
                            def calVal = Calendar.getInstance()
                            calVal.setTime(columnValue)
                            spreadSheetWriter.createCell(columnIndex, calVal, styles.get("date").getIndex())
                        } else if (columnValue instanceof Calendar) {
                            spreadSheetWriter.createCell(columnIndex, columnValue, styles.get("date").getIndex())
                        } else {
                            spreadSheetWriter.createCell(columnIndex, columnValue)
                        }
                    }
                } catch (Exception e) {
                    log.error("Error creating column", e)
                    throw e
                }
                break
        }

        return null
    }

    /**
     * This method isn't implemented.
     */
    protected Object createNode(Object name, Map attributes, Object value) {
        log.debug("createNode(Object name, Map attributes, Object value)")
        log.debug("name: ${name} attributes: ${attributes}, value: ${value}")
        return null
    }

    def static createStyles(XSSFWorkbook workbook) {
        Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>()
        XSSFDataFormat fmt = workbook.createDataFormat()

        styles.put("percent", createStyle(workbook, XSSFCellStyle.ALIGN_RIGHT, fmt.getFormat("0.0%")))
        styles.put("coeff", createStyle(workbook, XSSFCellStyle.ALIGN_CENTER, fmt.getFormat("0.0X")))
        styles.put("currency", createStyle(workbook, XSSFCellStyle.ALIGN_RIGHT, fmt.getFormat('$#,##0.00')))
        styles.put("date", createStyle(workbook, XSSFCellStyle.ALIGN_RIGHT, fmt.getFormat("mmm dd")))
        styles.put("header", createHeaderStyle(workbook))

        return styles
    }

    def static createStyle(XSSFWorkbook workbook, short styleType, short format) {
        XSSFCellStyle style = workbook.createCellStyle()

        style.setAlignment(styleType)
        style.setDataFormat(format)

        return style
    }

    def static createHeaderStyle(XSSFWorkbook workbook) {
        XSSFCellStyle style = workbook.createCellStyle()
        XSSFFont headerFont = workbook.createFont()

        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex())
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND)

        headerFont.setBold(true)
        style.setFont(headerFont)

        return style
    }

    def write() {
        // close the worksheet
        spreadSheetWriter.endSheet();

        // close the sheetWriter
        sheetWriter.close()

        // close zipOutputStream
        zos.close()

    }

    def static copyStream(InputStream inStream, OutputStream outStream) throws IOException {
        def chunk = new byte[1024]
        def count
        while ((count = inStream.read(chunk)) >= 0) {
            outStream.write(chunk, 0, count)
        }
    }
}