package de.andreasschmitt.export.builder

import org.apache.commons.lang.StringEscapeUtils
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.ss.util.CellReference

/**
 * Writes spreadsheet data in a Writer.
 * (YK: in future it may evolve in a full-featured API for streaming data in Excel)
 */
class SpreadsheetWriter {
    def static final String XML_ENCODING = "UTF-8"
    def final out
    def rowNum

    public SpreadsheetWriter(Writer out) {
        this.out = out
    }

    public void beginSheet() throws IOException {
        def template =
                """<?xml version="1.0" encoding="${XML_ENCODING}"?>
<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main">
   <sheetData>"""

        out.write(template)
    }

    public void endSheet() throws IOException {
        def template =
                """
   </sheetData>
</worksheet>"""
        out.write(template)
    }

    /**
     * Insert a new row
     *
     * @param rownum 0-based row number
     */
    public void insertRow(int rowNum) throws IOException {
        def template =
                """
      <row r="${rowNum + 1}">"""
        out.write(template)
        this.rowNum = rowNum
    }

    /**
     * Insert row end marker
     */
    public void endRow() throws IOException {
        def template =
                """
      </row>"""
        out.write(template)
    }

    public void createCell(int columnIndex, String value, int styleIndex) throws IOException {
        String ref = new CellReference(rowNum, columnIndex).formatAsString()
        def style = styleIndex != -1 ? " s=\"${styleIndex}\"" : ""

        // sanitize the string
        value = StringEscapeUtils.escapeXml(value)

        def columnTemplate =
                """
         <c r="${ref}" t="inlineStr"${style}>
            <is>
               <t>${value}</t>
            </is>
         </c>"""

        out.write(columnTemplate)
    }

    public void createCell(int columnIndex, String value) throws IOException {
        createCell(columnIndex, value, -1)
    }

    public void createCell(int columnIndex, double value, int styleIndex) throws IOException {
        String ref = new CellReference(rowNum, columnIndex).formatAsString()
        def style = styleIndex != -1 ? " s=\"${styleIndex}\"" : ""

        def columnTemplate =
                """
         <c r="${ref}" t="n" ${style}>
            <v>${value}</v>
         </c>"""

        out.write(columnTemplate)
    }

    public void createCell(int columnIndex, double value) throws IOException {
        createCell(columnIndex, value, -1)
    }

    public void createCell(int columnIndex, Calendar value, int styleIndex) throws IOException {
        createCell(columnIndex, DateUtil.getExcelDate(value, false), styleIndex)
    }
}
