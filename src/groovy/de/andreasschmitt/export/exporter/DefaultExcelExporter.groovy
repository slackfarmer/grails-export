package de.andreasschmitt.export.exporter

import de.andreasschmitt.export.builder.ExcelBuilder
import de.andreasschmitt.export.builder.BigExcelBuilder
import grails.util.Holders
import jxl.format.Colour

import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA
import org.springframework.beans.factory.annotation.Value

class DefaultExcelExporter extends AbstractExporter {
    def config = Holders.config

    protected void exportData(OutputStream outputStream, List data, List fields) throws ExportingException {
        try {

            def usePoi = config?.grails?.plugins?.export?.usePoi

            //check config to determine which excel library to use
            def builder

            if(usePoi){
                builder = new BigExcelBuilder()
            }else{
                builder = new ExcelBuilder()
            }

            // Enable/Disable header output
            boolean isHeaderEnabled = true
            if (getParameters().containsKey("header.enabled")) {
                isHeaderEnabled = getParameters().get("header.enabled")
            }

            boolean useZebraStyle = false
            if (getParameters().containsKey("zebraStyle.enabled")) {
                useZebraStyle = getParameters().get("zebraStyle.enabled")
            }

            builder {
                workbook(outputStream: outputStream) {
                    sheet(name: getParameters().get("title") ?: "Export", widths: getParameters().get("column.widths"), numberOfFields: data.size(), widthAutoSize: getParameters().get("column.width.autoSize")) {

                        format(name: "title") {
                            font(name: "arial", bold: true, size: 14)
                        }

                        format(name: "header") {
                            if (useZebraStyle) {
                                font(name: "arial", bold: true, backColor: Colour.GRAY_80, foreColor: Colour.WHITE, useBorder: true)
                            } else {
                                // Use default header format
                                font(name: "arial", bold: true)
                            }
                        }
                        format(name: "odd") {
                            font(backColor: Colour.GRAY_25, useBorder: true)
                        }
                        format(name: "even") {
                            font(backColor: Colour.WHITE, useBorder: true)
                        }

                        int rowIndex = 0

                        // Option for titles on top of data table
                        def titles = getParameters().get("titles")
                        titles.each {
                            if(usePoi) {
                                rowStart(row: rowIndex)
                            }
                            cell(row: rowIndex, column: 0, value: it, format: "title")
                            if(usePoi) {
                                rowEnd(row: rowIndex)
                            }
                            rowIndex++
                        }

                        //Create header
                        if (isHeaderEnabled) {
                            if(usePoi) {
                                rowStart(row: rowIndex)
                            }
                            fields.eachWithIndex { field, index ->
                                String value = getLabel(field)
                                cell(row: rowIndex, column: index, value: value, format: "header")
                            }
                            if(usePoi) {
                                rowEnd(row: rowIndex)
                            }
                            rowIndex++
                        }

                        //Rows
                        data.eachWithIndex { object, k ->
                            if(usePoi) {
                                rowStart(row: rowIndex)
                            }
                            String format = useZebraStyle ? ((k % 2) == 0 ? "even" : "odd") : ""
                            fields.eachWithIndex { field, i ->
                                Object value = getValue(object, field)
                                cell(row: k + rowIndex, column: i, value: value, format: format)
                            }
                            if(usePoi) {
                                rowEnd(row: rowIndex)
                            }
                            rowIndex++
                        }
                    }
                }
            }

            builder.write()
        }
        catch (Exception e) {
            throw new ExportingException("Error during export", e)
        }
    }

}